import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Cat> inventory = Arrays.asList(
                new Cat("Siberian", "white", 27),
                new Cat("Siberian", "whitesmoke", 14),
                new Cat("Singapura", "red", 34),
                new Cat("Somali", "dark", 4),
                new Cat("Korat", "black", 44),
                new Cat("Javanese", "white", 44)
        );

    }

    // filtering cats with variable color
    public static List<Cat> filterCatsByColor(List<Cat> inventory, String color) {
        List<Cat> result = new ArrayList(0);
        for (Cat cat : inventory) {
            if (cat.getColor().equals(color)) {
                result.add(cat);
            }
        }

        return result;
    }


    // filtering cats with variable color and breed
    public static List<Cat> filterCatsByColorAndBreed(List<Cat> inventory, String color, String breed) {
        List<Cat> result = new ArrayList(0);
        for (Cat cat : inventory) {
            if (cat.getColor().equals(color) && cat.getBreed().equals(breed)) {
                result.add(cat);
            }
        }

        return result;
    }

    public static class Cat {
        private String color;
        private String breed;
        private int age; // months

        public Cat(String color, String breed, int age) {
            this.color = color;
            this.breed = breed;
            this.age = age;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getBreed() {
            return breed;
        }

        public void setBreed(String breed) {
            this.breed = breed;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }
    }
}
