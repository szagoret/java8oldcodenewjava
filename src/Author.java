import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Created by szagoret on 23.09.2016.
 */
public class Author {

    private String name;
    private LocalDate birthday;
    private Integer awards;
    private Country country;
    private Optional<Country> optionalCountry;
    private List<Article> articles;

    public Author() {
    }

    public Author(String name, LocalDate birthday, Integer awards) {
        this.name = name;
        this.birthday = birthday;
        this.awards = awards;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Integer getAwards() {
        return awards;
    }

    public void setAwards(Integer awards) {
        this.awards = awards;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Optional<Country> getOptionalCountry() {
        return optionalCountry;
    }

    public void setOptionalCountry(Optional<Country> optionalCountry) {
        this.optionalCountry = optionalCountry;
    }

    public static int compareByAge(Author a1, Author a2) {
        return a1.getBirthday().compareTo(a2.getBirthday());
    }

    @Override
    public String toString() {
        return "Author{" +
                "name=" + name +
                ",birthday=" + birthday +
                ", awards=" + awards +
                "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Author author = (Author) o;
        return !(name != null ? !name.equals(author.name) : author.name != null);

    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
