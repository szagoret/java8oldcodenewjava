import java.io.File;
import java.io.FileFilter;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 * Created by szagoret on 23.09.2016.
 */
public class SlidesExamples {
    static Logger LOG = Logger.getLogger(SlidesExamples.class.getName());

    public static void main(String[] args) {
        // Java 7
        List<Author> authors = loadAuthors();
        Collections.sort(authors, new Comparator<Author>() {
            @Override
            public int compare(Author a1, Author a2) {
                return a1.getName().compareTo(a2.getName());
            }
        });

        Predicate<Author> authorPredicate = (t) -> false;
        Consumer<Author> authorConsumer = (t) -> {
            LOG.info("Consume an author");
        };
//        System.out.println();
//        authorConsumer.accept(new Author());

//        Author[] authorsArray = loadAuthorsArray();

//        Arrays.sort(authorsArray, new AuthorAgeComparator());

//        Arrays.sort(authorsArray, (a1, a2) -> a1.getBirthday().compareTo(a2.getBirthday()));

//        Arrays.sort(authorsArray, Author::compareByAge);


        Stream.iterate(0, n -> n + 2)
                .limit(10)
                .forEach(System.out::println);

        Stream.generate(Math::random)
                .limit(5)
                .forEach(System.out::println);
    }


    FileFilter fileFilter = new FileFilter() {
        @Override
        public boolean accept(File file) {
            return file.getName().endsWith(".java");
        }
    };

    static List<Author> loadAuthors() {
        return new ArrayList<>(0);
    }

    static Author[] loadAuthorsArray() {
        return new Author[10];
    }

    static class AuthorAgeComparator implements Comparator<Author> {
        @Override
        public int compare(Author a1, Author a2) {
            return 1;
//            return a1.getBirthday().compareTo(a2.getBirthday());
        }
    }

    interface Sized {
        int size();

        default boolean isEmpty() {
            return size() == 0;
        }
    }

    interface StructuredData {
        default void print(String text) {
            if (!isNull(text))
                System.out.println("StructuredData Print:" + text);
        }

        static boolean isNull(String text) {
            System.out.println("Interface Null Check");
            return text == null ? true : "".equals(text) ? true : false;
        }
    }


    static void optionalMethod(Author authorNull) {
        /**
         * Optional
         */
        // Optional from a non-null value
        //You can also create an optional from a non-null value with the static factory method Optional.of:
        Author authorNonNull = new Author();
        Optional<Author> nonNullAuthor = Optional.of(authorNonNull);

        //from null
        Optional<Author> nullAuthor = Optional.ofNullable(authorNull);

        // empty optional
        Optional<Author> emptyAuthor = Optional.empty();


    }

    static void newDateAPI() {
        Date date = new Date(114, 2, 18);
        Calendar calendar = new GregorianCalendar(2013, 1, 28, 13, 24, 56);

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH); // Jan = 0, dec = 11
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        int weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);
        int weekOfMonth = calendar.get(Calendar.WEEK_OF_MONTH);

        int hour = calendar.get(Calendar.HOUR);        // 12 hour clock
        int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY); // 24 hour clock
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);
        int millisecond = calendar.get(Calendar.MILLISECOND);
    }

}

/**
 * Java interface resolution rules
 */
interface A {
    default void hello() {
        System.out.println("Hello from A");
    }
}

interface B extends A {
    default void hello() {
        System.out.println("Hello from B");
    }
}

interface D extends A {
}

class E implements A, D {

}

class C extends E implements A, B {


    public static void main(String[] args) {
        new C().hello();
    }


    /**
     * Null - Optional
     */

    public static String getAuthorCountryName(Article article) {
        if (article.getPrimaryAuthor() == null) {
            Author author = article.getPrimaryAuthor();
            if (author.getCountry() != null) {
                Country country = author.getCountry();
                if (country.getName() != null) {
                    return country.getName();
                }
            }
        }
        return "Unknown";
    }


    public static String getAuthorCountryNameOptional(Article article) {
        return "";
    }
}
