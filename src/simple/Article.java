package simple;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Created by szagoret on 26.09.2016.
 */
public class Article {
    private String title;
    private String category;
    private LocalDate published;
    private Integer downloads;
    private List<Author> authors = new ArrayList<>(0);
    private Author primaryAuthor;

    public Article() {
    }

    public Article(String title, String category, LocalDate published, Integer downloads) {
        this.title = title;
        this.category = category;
        this.published = published;
        this.downloads = downloads;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public LocalDate getPublished() {
        return published;
    }

    public void setPublished(LocalDate published) {
        this.published = published;
    }

    public void setDownloads(Integer downloads) {
        this.downloads = downloads;
    }

    public Author getPrimaryAuthor() {
        return primaryAuthor;
    }

    public void setPrimaryAuthor(Author primaryAuthor) {
        this.primaryAuthor = primaryAuthor;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public Optional<Author> getOptionalAuthor() {
        return Optional.ofNullable(primaryAuthor);
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public Integer getDownloads() {
        return downloads;
    }

    public static Integer compareByDownloads(Article article1, Article article2) {
        return article1.getDownloads().compareTo(article2.getDownloads());
    }

    public void addAuthor(Author author) {
        this.authors.add(author);
    }

    public void addAuthors(Author... author) {
        Arrays.asList(author).stream().forEach(this::addAuthor);
    }

    @Override
    public String toString() {
        return "\nArticle{" +
                "\n\ttitle='" + title + '\'' +
                "\n\tcategory='" + category + '\'' +
                "\n\tpublished=" + published +
                "\n\tauthors=" + authors +
                "\n}";
    }
}
