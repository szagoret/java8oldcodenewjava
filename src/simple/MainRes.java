package simple;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by szagoret on 29.09.2016.
 */
public class MainRes {

    public static void main(String[] args) {
        List<Article> articles = DataSource.getArticles();

        /**
         * flatMap
         * get unique authors names
         */
        articles.stream().map(a -> a.getAuthors())
                .flatMap(authors->authors.stream())
                .map(author -> author.getName())
                .distinct()
                .forEach(System.out::println);
        System.out.println("ends author list");

        /**
         *  Find all articles with 2+ authors
         */
        // Java 7
        List<Article> byDownloads = new ArrayList<>();
        for (Article article : articles) {
            if (article.getAuthors().size() > 1) {
                byDownloads.add(article);
            }
        }
        System.out.println("Result count: " + byDownloads.size());
        //Java 8
        List<Article> byDownloadsStream = articles.stream()
                .filter((a) -> a.getAuthors().size() > 1).collect(Collectors.toList());

        System.out.println("Result stream count: " + byDownloadsStream.size());
        System.out.println("Find by Category: " + findByCategoryOrDownloads(articles, "Tech", 0, true).size());

        Predicate<Article> findByCategory = article -> article.getCategory().equals("Tech");
        System.out.println("Find by Category predicate: " + findArticles(articles, findByCategory).size());


        /**
         * Get unique Authors, sorted by name asc
         */
        // Java 7
        Set<Author> setAuthors = new HashSet<>();
        for (Article article : articles) {
            for (Author author : article.getAuthors()) {
                setAuthors.add(author);
            }
        }
        List<Author> listAuthors = new ArrayList<>(setAuthors);
        Collections.sort(listAuthors, Collections.reverseOrder(new Comparator<Author>() {
            @Override
            public int compare(Author a1, Author a2) {
                return a2.getName().compareTo(a1.getName());
            }
        }));
        System.out.println("Unique authors: " + listAuthors);
        System.out.println("Unique authors count: " + listAuthors.size());

        //Java 8 Unique authors sorted by names
        List<Author> sortedAuthors = articles.stream().map(a -> a.getAuthors())
                .flatMap(authors -> authors.stream())
                .distinct()
                .sorted(Comparator.comparing(Author::getName, Comparator.reverseOrder()))
                .collect(Collectors.toList());
        System.out.println("Java 8");
        System.out.println("Unique authors: " + sortedAuthors);
        System.out.println("Unique authors count: " + sortedAuthors.size());

        /**
         * Get top 5 authors sorted by awards
         * if number of awards is equals then sort by name asc
         */

        // Java 7
        setAuthors = new HashSet<>();
        for (Article article : articles) {
            for (Author author : article.getAuthors()) {
                setAuthors.add(author);
            }
        }
        listAuthors = new ArrayList<>(setAuthors);
        Collections.sort(listAuthors, Collections.reverseOrder(new Comparator<Author>() {
            @Override
            public int compare(Author a1, Author a2) {
                int byAwards = a1.getAwards().compareTo(a2.getAwards());
                if (byAwards == 0) {
                    return a2.getName().compareTo(a1.getName());
                } else {
                    return byAwards;
                }
            }
        }));

        List<Author> topFiveAuthors = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            topFiveAuthors.add(listAuthors.get(i));
        }

        System.out.println("Authors count : " + listAuthors.size());
        System.out.println("Top 5 authors: " + topFiveAuthors);

        // Java 8
        List<Author> top5AuthorsLambda = articles.stream().map((a) -> a.getAuthors())
                .flatMap((authors) -> authors.stream())
                .distinct()
                .sorted(Comparator.comparing(Author::getAwards, Comparator.reverseOrder())
                        .thenComparing(Author::getName))
                .limit(5)
                .collect(Collectors.toList());
        System.out.println("Lambda Top 5 authors: " + top5AuthorsLambda);

        /**
         * composing functions
         */
        Comparator<Author> authorComparatorByAwards = (author1, author2) -> author1.getAwards().compareTo(author2.getAwards());
        Comparator<Author> authorComparatorByName = (author1, author2) -> author1.getName().compareTo(author2.getName());

        Comparator<Author> compareByAwardsAndName = authorComparatorByAwards.thenComparing(authorComparatorByName);

        Comparator<Author> byAwardsReversed = Comparator.comparing(Author::getAwards, Comparator.reverseOrder());
        Comparator<Author> byName = (a1, a2) -> a1.getName().compareTo(a2.getName());
        Comparator<Author> byAwardsReversedAndName = byAwardsReversed.thenComparing(byName);

    }

    public static List<Article> findArticles(List<Article> articles, Predicate<Article> articlePredicate) {
        List<Article> result = new ArrayList<>();
        for (Article article : articles) {
            if (articlePredicate.test(article)) {
                result.add(article);
            }
        }
        return result;
    }


    public static List<Article> findByCategoryOrDownloads(List<Article> articles, String category,
                                                          int downloads, boolean flag) {
        List<Article> result = new ArrayList<>();
        for (Article article : articles) {
            if (flag && article.getCategory().equals(category)
                    || !flag && article.getDownloads() > downloads) {
                result.add(article);
            }
        }
        return result;
    }
}

/**
 * Java interface resolution rules
 */
interface A {
    default void hello() {
        System.out.println("Hello from A");
    }
}

interface B extends A {
    default void hello() {
        System.out.println("Hello from B");
    }
}

interface D extends A {
}

class E implements A, D {

}

class C extends E implements A, B {


    public static void main(String[] args) {
        new C().hello();
    }

}
