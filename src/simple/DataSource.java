package simple;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

/**
 * Created by szagoret on 26.09.2016.
 */
public class DataSource {

    private static List<Article> articles;

    static {
        /**
         * Authors
         */
        Author james = new Author("James", LocalDate.of(1960, 1, 30), 20);     //1
        Author jacob = new Author("Jacob", LocalDate.of(1978, 12, 15), 4);     //2
        Author sophie = new Author("Sophie", LocalDate.of(1988, 7, 11), 14);   //3
        Author henry = new Author("Henry", LocalDate.of(1990, 3, 5), 0);       //4
        Author leo = new Author("Leo", LocalDate.of(1991, 2, 1), 3);           //5
        Author theo = new Author("Theo", LocalDate.of(1987, 7, 9), 12);        //6
        Author alice = new Author("Alice", LocalDate.of(1991, 4, 10), 14);      //7

        /**
         * Articles
         */
        Article teleportation = new Article("Quantum Teleportation ", "Tech", LocalDate.of(2016, 2, 3), 4); //1,2
        teleportation.addAuthors(james, jacob);

        Article electricBus = new Article("Electric Bus ", "Tech", LocalDate.of(2014, 1, 9), 8); //2
        electricBus.addAuthor(jacob);

        Article wearableCamera = new Article("Wearable Camera", "Tech", LocalDate.of(2014, 10, 5), 100); //3
        wearableCamera.addAuthor(sophie);

        Article futuristicTechnologies = new Article("10 Futuristic Technologies", "Tech", LocalDate.of(2016, 2, 3), 56); //3,5
        futuristicTechnologies.addAuthors(sophie, leo);

        Article antibioticResistantBacteria = new Article("Antibiotic-Resistant Bacteria", "Health", LocalDate.of(2016, 2, 3), 56); //4
        antibioticResistantBacteria.addAuthor(henry);

        Article medicalAlert = new Article("Best Medical Alert Systems 2016", "Health", LocalDate.of(2016, 6, 14), 98); //5
        medicalAlert.addAuthor(leo);

        Article anxietySensitivity = new Article("What Is Anxiety Sensitivity, and Do I Have It?", "Mind", LocalDate.of(2013, 9, 11), 400); //6,1
        anxietySensitivity.addAuthors(theo, james);

        Article moralityForeignLanguage = new Article("How Morality Changes in a Foreign Language", "Mind", LocalDate.of(2015, 5, 5), 1000); //7
        moralityForeignLanguage.addAuthor(alice);

        Article geoglyphsPeru = new Article("Ring-Shaped Geoglyphs Found Near Ancient Town in Peru", "History", LocalDate.of(2016, 9, 23), 256); //7
        geoglyphsPeru.addAuthor(alice);

        Article ancientRomanScotland = new Article("Remains of Ancient Roman Oven Unearthed in Scotland", "History", LocalDate.of(2016, 9, 23), 125); //4,5
        ancientRomanScotland.addAuthors(henry, leo);

        Article goldCoinNeroFace = new Article("Rare Gold Coin with Nero's Face Discovered in Jerusalem", "History", LocalDate.of(2016, 9, 14), 111); //1,2,3
        goldCoinNeroFace.addAuthors(james, jacob, sophie);

        /**
         * Add articles to list
         */
        articles = Arrays.asList(teleportation, electricBus, wearableCamera, futuristicTechnologies, antibioticResistantBacteria,
                medicalAlert, anxietySensitivity, moralityForeignLanguage, geoglyphsPeru, ancientRomanScotland, goldCoinNeroFace);
    }

    public static List<Article> getArticles() {
        return articles;
    }

    public static Author getNullAuthor() {
        return null;
    }

    public static Article getArticle(){
        return articles.get(1);
    }
}
