package simple;

import java.util.Optional;

/**
 * Created by szagoret on 28.09.2016.
 */
public class Country {
    private String name;
    private String code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Optional<String> getNameAsOptional() {
        return Optional.ofNullable(name);
    }
}
