import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Created by szagoret on 28.09.2016.
 */
public class Simple {


    public static void main(String[] args) {

        List<Article> articles = DataSource.getArticles();
        Collections.sort(articles, (a1, a2) -> a1.getTitle().compareTo(a2.getTitle()));

        Function<Article, String> authorToString =
                a -> a.getTitle();
        Predicate<Article> articlePredicate = (a) -> true;
        myF(authorToString);

        Supplier<Article> getArticle = Article::new;
    }

    public static void myF(Function<Article, String> authorToString){}


}
interface A1{
    default void sayHello(){
        System.out.println("Hello from A1");
    }
}
interface B1{
    default void sayHello(){
        System.out.println("Hello from B1");
    }
}

class Alpha implements B1{}
