import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.Year;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Created by szagoret on 23.09.2016.
 */
public class App {

    public static void main(String[] args) {

        List<Article> articles = DataSource.getArticles();


        // Compare by downloads
        Collections.sort(articles, (a1, a2) -> a1.getDownloads().compareTo(a2.getDownloads()));
        Collections.sort(articles, (a1, a2) -> a1.getDownloads().compareTo(a2.getDownloads()));
        /**
         *  Find all articles with 2+ authors
         */

        // Java 7
        List<Article> result7 = new ArrayList<>();
        for (Article article : articles) {
            if (article.getAuthors().size() > 1) {
                result7.add(article);
            }
        }

        //Java 8
        List<Article> result8 = articles.stream().filter((a) -> a.getAuthors().size() > 1).collect(Collectors.toList());

        System.out.println("Found " + result8.size() + " articles.");
        System.out.println(result8);


        /**
         * Find articles titles:
         *  1. more than 50 downloads
         *  2. author has more than 5 awards
         */

        // Java 7
        List<String> articlesResult1 = new ArrayList<>();
        for (Article article : articles) {
            if (article.getDownloads() > 50) {
                articlesResult1.add(article.getTitle());
            }
        }
        System.out.println("Java 7 found: " + articlesResult1.size() + " articles.");
        System.out.println(articlesResult1);

        // Java 8
        List<String> articlesResult2 = articles.stream().filter((a) -> a.getDownloads() > 50)
                .map((a) -> a.getTitle()).collect(Collectors.toList());
        System.out.println("Java 8 found: " + articlesResult2.size() + " articles.");
        System.out.println(articlesResult2);

        /**
         * Find articles titles:
         *  1. more than 50 downloads
         */
        List<Article> moreThan50 = findArticlesByDownloads(articles, 50);
        System.out.println("Found: " + articlesResult2.size() + " articles.");
        System.out.println(articlesResult2);

        /**
         * Find articles by category:
         */
        List<Article> findByCategory = findArticlesByDownloadsOrCategory(articles, null, "Mind", false);
        System.out.println("Find By Category: " + findByCategory.size() + " articles.");
        System.out.println(findByCategory);

        /**
         * Use predicate
         */

        List<Article> findArticleByPredicate = findArticles(articles, (p) -> p.getCategory().equals("Mind"));
        System.out.println("Find By Predicate: " + findArticleByPredicate.size() + " articles.");
        System.out.println(findArticleByPredicate);


        /**
         * Get top 5 authors sorted by awards
         * if number of awards is equals then sort by name asc
         */

        // Java 7
        Set<Author> setAuthors = new HashSet<>();
        for (Article article : articles) {
            for (Author author : article.getAuthors()) {
                setAuthors.add(author);
            }
        }
        List<Author> listAuthors = new ArrayList<>(setAuthors);
        Collections.sort(listAuthors, Collections.reverseOrder(new Comparator<Author>() {
            @Override
            public int compare(Author a1, Author a2) {
                int byAwards = a1.getAwards().compareTo(a2.getAwards());
                if (byAwards == 0) {
                    return a2.getName().compareTo(a1.getName());
                } else {
                    return byAwards;
                }
            }
        }));

        List<Author> topThreeAuthors = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            topThreeAuthors.add(listAuthors.get(i));
        }

        System.out.println("Authors count : " + listAuthors.size());
        System.out.println("Top 5 authors: " + topThreeAuthors);

        // Java 8
        Comparator<Author> authorComparatorByAwards = (author1, author2) -> author1.getAwards().compareTo(author2.getAwards());
        Comparator<Author> authorComparatorByName = (author1, author2) -> author1.getName().compareTo(author2.getName());

        Comparator<Author> compareByAwardsAndName = authorComparatorByAwards.thenComparing(authorComparatorByName);

        Comparator<Author> byAwardsReversed = Comparator.comparing(Author::getAwards, Comparator.reverseOrder());
        Comparator<Author> byName = (a1, a2) -> a1.getName().compareTo(a2.getName());
        Comparator<Author> byAwardsReversedAndName = byAwardsReversed.thenComparing(byName);

        List<Author> top5AuthorsLambda = articles.stream().map((a) -> a.getAuthors())
                .flatMap((authors) -> authors.stream())
                .distinct()
                .sorted(Comparator.comparing(Author::getAwards, Comparator.reverseOrder())
                        .thenComparing(Author::getName))
                .limit(5)
                .collect(Collectors.toList());
        System.out.println("Lambda Top 5 authors: " + top5AuthorsLambda);

        Supplier<Author> supplier = () -> new Author();
        Supplier<Author> supplier1 = Author::new;
        supplier.get();


        /**
         * Optional
         */

        // Optional from a non-null value
        //You can also create an optional from a non-null value with the static factory method Optional.of:
        Author authorNonNull = new Author();
        Optional<Author> nonNullAuthor = Optional.of(authorNonNull);

        //from null
        Author authorNull = DataSource.getNullAuthor();
        Optional<Author> nullAuthor = Optional.ofNullable(authorNull);

        // empty optional
        Optional<Author> emptyAuthor = Optional.empty();

        Optional<Article> article = Optional.ofNullable(DataSource.getArticle());
        Optional<Author> primaryAuthor = article.map(Article::getPrimaryAuthor);
        System.out.println(primaryAuthor);

        String countryName = article
                .flatMap(Article::getOptionalAuthor)
                .flatMap(Author::getOptionalCountry)
                .map(Country::getName).orElse("Unknown");
        System.out.println("Country name:" + countryName);

        article.isPresent();
        article.get();

        /**
         * Find articles with publication year > 2014
         */
        LocalDateTime now = LocalDateTime.now();

        long count = articles.stream().filter(a -> a.getPublished().isAfter(LocalDate.ofYearDay(2014, Year.of(2014).length())))
                .count();
        System.out.println("Filter by date: " + count);
        System.out.println("Between: " + Period.between(LocalDate.now(), LocalDate.of(2015, 3, 2)));

        Function<Author, String> authorStringFunction = (author) -> {return author.getName();};
    }

    public static List<Article> findArticlesByDownloads(List<Article> articles, Integer downloads) {
        List<Article> result = new ArrayList<>();
        for (Article article : articles) {

            if (article.getDownloads() >= downloads) {
                result.add(article);
            }
            ///some code

        }
        return result;
    }

    public static List<Article> findArticles(List<Article> articles, Predicate<Article> articlePredicate) {
        List<Article> result = new ArrayList<>();
        for (Article article : articles) {
            if (articlePredicate.test(article)) {
                result.add(article);
            }
        }
        return result;
//        return articles.stream().filter(articlePredicate).collect(Collectors.toList());
    }


    public static List<Article> findArticlesByDownloadsOrCategory(List<Article> articles, Integer downloads,
                                                                  String category, Boolean isByDownloads) {
        List<Article> result = new ArrayList<>();
        for (Article article : articles) {
            // security
            // ....
            if (isByDownloads && article.getDownloads() >= downloads ||
                    !isByDownloads && article.getCategory().equals(category)) {
                result.add(article);
            }
            // ...
        }
        return result;
    }


}
