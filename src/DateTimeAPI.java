/**
 * Created by szagoret on 28.09.2016.
 */

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAdjuster;
import java.util.Date;
import java.util.Locale;

/**
 * @author szagoret
 */
public class DateTimeAPI {

    public static void main(String[] args) {
        LocalDate date = LocalDate.of(2014, 3, 18); // 2014-03-18
        int year = date.getYear(); // 2014
        Month month = date.getMonth(); // MARCH
        int day = date.getDayOfMonth(); // 18
        DayOfWeek dow = date.getDayOfWeek(); // TUESDAY
        int len = date.lengthOfMonth(); //31 (days in March)
        boolean leap = date.isLeapYear();

        LocalDate today = LocalDate.now(); // current date from system clock

        /**
         * The TemporalField is an interface defining how to access the value of a specific field of
         * a temporal object. The ChronoField enumeration implements this interface, so you can
         * conveniently use an element of that enumeration with the get method, as shown in the next
         * listing.
         */
        int chronoYear = date.get(ChronoField.YEAR);
        int chronoMonth = date.get(ChronoField.MONTH_OF_YEAR);
        int chronoDay = date.get(ChronoField.DAY_OF_MONTH);

        // LocalTime
        LocalTime time = LocalTime.of(13, 45, 20); // 13:45:20
        int hour = time.getHour(); //13
        int minute = time.getMinute(); // 45
        int second = time.getSecond(); //20

        // Both LocalDate and LocalTime can be created by parsing a String representing them
        LocalDate dateFromString = LocalDate.parse("2014-03-18");
        LocalTime timeFromString = LocalTime.parse("13:45:20");

        System.out.printf("Local date are %s and time %s \n", dateFromString, timeFromString);

        //Creating a LocalDateTime directly or by combining a date and a time
        // 2014-03-18T13-45-20
        LocalDateTime dt0 = LocalDateTime.of(2015, Month.MAY, 8, 3, 4, 2);
        LocalDateTime dt1 = LocalDateTime.of(2014, Month.MARCH, 18, 13, 45, 20);
        LocalDateTime dt2 = LocalDateTime.of(date, time);
        LocalDateTime dt3 = date.atTime(time);
        LocalDateTime dt4 = time.atDate(date);

        // extract LocalTime or LocalDate
        LocalDate date1 = dt1.toLocalDate(); // 2014-03-18
        LocalTime time1 = dt1.toLocalTime(); // 13:45:20

        // All 4 are exactly the same Instant
        Instant instant1 = Instant.ofEpochSecond(3);
        Instant instant2 = Instant.ofEpochSecond(3, 0);
        Instant.ofEpochSecond(2, 1000000000); // One bilion nanoseconds (1 second) after 2 seconds
        Instant.ofEpochSecond(4, -1000000000);// One bilion nanoseconds (1 second) before 4 seconds

        /**
         * It doesn�t provide any ability to handle units of time that are meaningful to humans.
         * java.time.temporal.UnsupportedTemporalTypeException: Unsupported field: DayOfMonth
         */
        // int day = Instant.now().get(ChronoField.DAY_OF_MONTH);
        Duration d1 = Duration.between(instant1, instant2);
        Duration d2 = Duration.between(dt0, dt1);

        /**
         * Because LocalDateTime and Instant are made for different purposes, one to be used by
         * humans and the other by machines, you�re not allowed to mix them. If you try to create a
         * duration between them, you�ll only obtain a DateTimeException. Moreover, because the
         * Duration class is used to represent an amount of time measured in seconds and eventually
         * nanoseconds, you can�t pass a LocalDate to the between method. When you need to model an
         * amount of time in terms of years, months, and days, you can use the Period class. You can
         * find out the difference between two LocalDates with the between factory method of that
         * class:
         */
        Period tenDays = Period.between(LocalDate.of(2014, 3, 8), LocalDate.of(2014, 3, 18));
        System.out.println("Ten days period: " + tenDays.get(ChronoUnit.DAYS));

        // Creating Durations an Periods
        Duration threeMinutes = Duration.ofMinutes(3);
        Duration threeMinutes1 = Duration.of(3, ChronoUnit.MINUTES);
        Period tenDays1 = Period.ofDays(10);
        Period tenWeeks = Period.ofWeeks(3);
        Period twoYearsSixMonthOneDay = Period.of(2, 6, 1);

        LocalTime localTimeInVladivostok = ZonedDateTime.now(ZoneId.of("Asia/Vladivostok")).toLocalTime();
        System.out.println("localTimeInVladivostok: "+localTimeInVladivostok);
        /**
         * Quiz
         */
        LocalDate nextWorkingDay = LocalDate.of(2015, Month.JUNE, 12);
        System.out.printf("Next working day of %s is %s \n",
                nextWorkingDay, nextWorkingDay.with(new NextWorkingDay()));

        /**
         * Printing an parsing date-time objects
         */
        LocalDate date2 = LocalDate.of(2014, 3, 18);
        LocalDate localDateFromDate = LocalDateTime.ofInstant(Instant.ofEpochMilli(new Date().getTime()), ZoneId.systemDefault()).toLocalDate();
        Date dateFromLocalDate = Date.from(localDateFromDate.atStartOfDay(ZoneId.systemDefault()).toInstant());

        String s1 = date2.format(DateTimeFormatter.BASIC_ISO_DATE); // 20140318
        String s2 = date2.format(DateTimeFormatter.ISO_LOCAL_DATE); // 2014-03-18
        String s3 = date2.format(DateTimeFormatter.ISO_WEEK_DATE); // 2014-03-18T00:00:00
        System.out.println("BASIC_ISO_DATE: " + s1);
        System.out.println("ISO_LOCAL_DATE: " + s2);
        System.out.println("ISO_WEEK_DATE: " + s3);

        // Creating a DateTimeFormater from a pattern
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyy");
        LocalDate date6 = LocalDate.of(2014, Month.MARCH, 18);
        String formattedDate = date6.format(formatter);
        LocalDate date7 = LocalDate.parse(formattedDate, formatter);
        System.out.println("Parsed date: " + date7);

        // Creating a localized DateTimeFormatter
        DateTimeFormatter italianFormatter = DateTimeFormatter.ofPattern("d.MMMM yyyy", Locale.ITALIAN);
        LocalDate date3 = LocalDate.of(2014, Month.MARCH, 18);
        String formattedDateItalian = date.format(italianFormatter); //18.marzo 2014
        System.out.println("Italian date: " + formattedDateItalian);
        LocalDate date4 = LocalDate.parse(formattedDateItalian, italianFormatter); // 2014-03-18
        System.out.println("Parsed Italian date:" + date4);

        // Programmaticaly build a formatter
        DateTimeFormatter myItalianFormatter = new DateTimeFormatterBuilder()
                .appendText(ChronoField.DAY_OF_MONTH)
                .appendLiteral(".")
                .appendText(ChronoField.MONTH_OF_YEAR)
                .appendLiteral(" ")
                .appendText(ChronoField.YEAR)
                .parseCaseInsensitive()
                .toFormatter(Locale.ITALIAN);
        LocalDate date5 = LocalDate.parse(formattedDateItalian, myItalianFormatter); // 2014-03-18
        System.out.println("Parsed My Italian date:" + date5);
    }

    private static class NextWorkingDay implements TemporalAdjuster {

        @Override
        public Temporal adjustInto(Temporal temporal) {
            // read the current day
            DayOfWeek dayOfWeek = DayOfWeek.of(temporal.get(ChronoField.DAY_OF_WEEK));
            // Normally add one day and
            int dayToAdd = 1;
            if (dayOfWeek == DayOfWeek.FRIDAY) {
                dayToAdd = 3;
            } else if (dayOfWeek == DayOfWeek.SATURDAY) {
                dayToAdd = 2;
            }
            return temporal.plus(dayToAdd, ChronoUnit.DAYS);
        }
    }
}